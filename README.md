**Дипломный проект. Мониторинг.<br/>**

1. Установка Elasticsearch<br/>

В /etc/hosts на сервере мониторинга добавить:<br/>
127.0.0.1 localhost<br/>
Загрузить и установить elasticsearch:<br/>
curl -L -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.1.2-amd64.deb<br/>
sudo dpkg -i elasticsearch-8.1.2-amd64.deb<br/>

Запуск:<br/>
sudo systemctl enable elasticsearch.service<br/><br/>
Изменить таймаут старта сервиса в файле /etc/systemd/system/multi-user.target.wants:<br/> 
TimeoutStartSec=180<br/>

sudo systemctl daemon-reload<br/>
sudo systemctl start elasticsearch.service<br/>

Выполнить проверку командой curl http://127.0.0.1:9200<br/>

Для сброса пароля учетной записи администратора:<br/>
/usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic -i<br/>
Создание токена для Кибаны<br/>
/usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana<br/>

2. Установка Kibana<br/>

curl -L -O https://artifacts.elastic.co/downloads/elasticsearch/kibana-8.1.2-amd64.deb<br/>
dpkg -i kibana-8.1.2-amd64.deb<br/>

В каталог /etc/kibana скопировать файл http_ca.crt из каталога /etc/elasticsearch/certs<br/>
Поправить в /etc/kibana/kibana.yml:<br/>
‘server.host: "localhost”’ на ‘server.host: "0.0.0.0"’,<br/>
чтобы она была доступна не только на локальной машине.<br/>
И в поле elasticsearch.hosts:<br/>
elasticsearch.hosts: ['https://localhost:9200']<br/>
…
elasticsearch.ssl.certificateAuthorities: [ /etc/kibana/http_ca.crt ]<br/>

./kibana-setup --enrollment-token ..токен сгенерированный выше в пункте 1..<br/>
systemctl restart kibana<br/>
Зайти в браузере на порт 5601 — там должен быть веб-интерфейс Kibana.<br/>

3. Установка Filebeat<br/>

Используется чарт из каталога filebeat.<br/>
Создать на управляющей ноде неймспейс monitoring<br/>
    kubectl create ns monitoring<br/>
Скопировать ca-сертификат elastic (например, elasticsearch-ca.pem) и выполнить<br/>
    kubectl create secret generic elastic-ca --from-file=elasticsearch-ca.pem -n monitoring<br/>
В файле values.yaml helm-чарта filebeat исправить параметры подключения к elastic и kibana в секции extraEnvs<br/>
Затем установить чарт filebeat скопировав его на master-ноду кластера<br/>
    helm install diplom-filebeat filebeat -n monitoring<br/>

4. Установка на управляющую ноду node-exporter<br/>

Добавить репозиторий:<br/>
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts<br/>
Установить чарт prometheus-node-exporter<br/>
helm install prometheus-node-exporter prometheus-community/prometheus-node-exporter --version 3.3.0 -n monitoring<br/>

5. Установка prometheus-стека<br/>

Скопировать на сервер содержимое каталога prometheus_stack.<br/>
В файле docker-compose.yml в секции alertmanager-bot в параметре alertmanager.url указать внешний адрес сервера мониторинга.<br/>
В параметрах TELEGRAM_ADMIN и TELEGRAM_TOKEN указать идентификатор администратора (@userinfobot) и токен канала.<br/>
В файле prometheus/prometheus.yml для джоба nodeexporter_node1 указать внутренний ip адрес управляющей ноды. Для джоба nodeexporter_node2 указать внутренний ip адрес сервера мониторинга. Для джоба blackbox в параметре targets указать интернет ip адрес сетевого балансировщика.<br/>
В файле alertmanager/config.yml для alertmanager-bot указать интернет адрес сетевого балансировщика, на котором должен быть указано подключение по порту 8081 к серверу мониторинга.
